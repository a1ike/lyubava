jQuery(function ($) {
  document.querySelectorAll('a[href^="#"]').forEach((anchor) => {
    anchor.addEventListener('click', function (e) {
      e.preventDefault();

      document.querySelector(this.getAttribute('href')).scrollIntoView({
        behavior: 'smooth',
      });
    });
  });

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('.open-modal').on('click', function (e) {
    $('.l-modal').toggle();
  });

  $('.l-modal__centered').on('click', function (e) {
    if (e.target.className === 'l-modal__centered') {
      $('.l-modal').hide();
    }
  });

  $('.l-modal__close').on('click', function (e) {
    $('.l-modal').hide();
  });

  $('.l-header__mob').on('click', function (e) {
    e.preventDefault();

    $('.l-header__nav').slideToggle('fast');
    $('.l-header__info').slideToggle('fast');
  });

  new Swiper('.l-home-top__cards', {
    pagination: {
      el: '.l-home-top .swiper-pagination',
      clickable: true,
      renderBullet: function (index, className) {
        return '<span class="' + className + '">' + (index + 1) + '</span>';
      },
    },
    navigation: {
      nextEl: '.l-home-top .swiper-button-next',
      prevEl: '.l-home-top .swiper-button-prev',
    },
    slidesPerView: 1,
    loop: true,
    loopedSlides: 10,
    spaceBetween: 150,
  });

  new Swiper('.l-home-clients__cards', {
    autoplay: {
      delay: 2500,
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 5,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 10,
      },
    },
  });

  new Swiper('.l-home-papers__cards', {
    loop: true,
    loopedSlides: 10,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 5,
        loop: false,
      },
    },
  });
});
